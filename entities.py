from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa

Base = declarative_base()

class Part(Base):
    __tablename__ = "parts"

    id = sa.Column("id", sa.Integer, primary_key=True)
    name = sa.Column("name", sa.String(50))


class Person(Base):
    __tablename__ = "persons"

    id = sa.Column("id", sa.String, primary_key=True, autoincrement=False)
    gender = sa.Column("gender", sa.String(20))
    age = sa.Column("age", sa.Integer)

class Trace(Base):
    __tablename__ = "traces"

    id = sa.Column("id", sa.Integer, primary_key=True, autoincrement=True)
    person_id = sa.Column("person_id", sa.String, sa.ForeignKey("persons.id"))
    part_id = sa.Column("part_id", sa.Integer, sa.ForeignKey("parts.id"))
    max_emotion = sa.Column("max_emotion", sa.String)
    predicted_age = sa.Column("predicted_age", sa.Integer)
    predicted_gender = sa.Column("predicted_gender", sa.String)
    base64image = sa.Column("face_image", sa.String)
    time_code = sa.Time()
