import sqlalchemy as sa
import glob
import os
from io import BytesIO
from entities import Part, Person, Trace
import json
import base64
from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker
from PIL import Image
import click

DATA_FOLDER = "."
engine = sa.create_engine("postgresql+psycopg2://postgres:postgres@localhost:5432/conference")
metadata = sa.MetaData()




def in_data_folder(path):
    return os.path.join(DATA_FOLDER, path)

def get_last_dir(path):
    path = os.path.normpath(path).split(os.path.sep)
    path = path[len(path) - 1]
    return path

def get_time_from_seconds(seconds):
    dt = datetime.min + timedelta(0, seconds)
    return dt.time()

Session = sessionmaker(bind=engine)
dbsession = Session()

def parse_json_files():
    part_paths = glob.glob("labels/**/")
    for part_path in part_paths:
        print("Working on {}...".format(part_path))
        current_part_name = get_last_dir(part_path)
        current_part_id = dbsession.query(Part).filter(Part.name == current_part_name).first().id
        json_list = glob.glob(part_path + "**/*.json")

        with click.progressbar(json_list) as pbar:
            for jfile in pbar:
                with open(jfile) as file:
                    get_time_from_filename(jfile)
                    data = json.load(file)
                    traces = data['results']
                    image_path = data["imgPath"]
                    for trace in traces:
                        dbtrace = create_dbtrace(trace, image_path, current_part_id)
                        dbsession.add(dbtrace)

        dbsession.commit()

def create_parts():
    part_paths = glob.glob("labels/**/")
    for part_path in part_paths:
        part_name = get_last_dir(part_path)
        new_part = Part(name=part_name)
        dbsession.add(new_part)
        dbsession.commit()

def get_person_by_id(id):
    return dbsession.query(Person).get(id)



def create_dbtrace(trace, image_path, current_part_id):
    face_attr = trace["faceAttributes"]
    max_emotion = max([
        (value, key) for key, value in face_attr['emotion'].items()
    ])[1]
    person = get_person_by_id(trace["faceId"])
    if person is None:
        dbsession.add(Person(id=trace["faceId"]))
        dbsession.commit()

    image = Image.open(in_data_folder(image_path))
    face_image = crop_face(trace["faceRectangle"], image)
    base64_face_image = image2base64(face_image)
    dbtrace = Trace(person_id=trace["faceId"], 
        predicted_gender=face_attr['gender'], 
        predicted_age=face_attr['age'], 
        max_emotion=max_emotion, 
        part_id=current_part_id,
        base64image=base64_face_image)
    return dbtrace

def image2base64(image):
    buffer = BytesIO()
    image.save(buffer, format="JPEG")
    base64_face_image = base64.b64encode(buffer.getvalue())
    return base64_face_image.decode("utf-8")

def crop_face(rect, image):
    face_image = image.crop((
        rect["left"],
        rect["top"],
        rect["left"] + rect["width"],
        rect["top"] + rect["height"]
    ))
    return face_image

def get_time_from_filename(jfile):
    time_code_in_seconds = int(jfile.split(".")[0].split("_")[-1])
    time_code = get_time_from_seconds(time_code_in_seconds)

create_parts()
parse_json_files()
